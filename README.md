# Mesa de Órdenes CDC

![logo](https://www.vectorlogo.es/wp-content/uploads/2019/04/logo-vector-cinepolis.jpg)

## Desarrolladores

* [@Adrián Moreno](mailto:amorenoc@cinepolis.com) 🦍
* [@Pake Valencia](mailto:fjvalencia@cinepolis.com)

## Licencia
Cinépolis de México, S.A. de C.V