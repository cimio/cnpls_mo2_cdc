package com.cinepolis.mo.cdc.ServiceTest;

import com.cinepolis.mo.cdc.model.Version;
import com.cinepolis.mo.cdc.repository.VersionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class VersionServiceTest {

    @Autowired
    private VersionRepository versionRepository;

    @Test
    void lastOne() {
        Version version = versionRepository.lastOne();
        assertNotNull(version);
    }
}