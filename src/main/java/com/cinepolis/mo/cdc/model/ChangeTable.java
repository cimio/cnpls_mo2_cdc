package com.cinepolis.mo.cdc.model;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "CHANGETABLE(CHANGES tblItemOrderStatus,0)")
public class ChangeTable implements Serializable {

    @Id
    @Column(name = "SYS_CHANGE_VERSION")
    private Long version;

    @Column(name = "SYS_CHANGE_CREATION_VERSION")
    private String changeCreateVersion;

    @Column(name = "SYS_CHANGE_OPERATION")
    private Character changeOperation;

    @Column(name = "SYS_CHANGE_COLUMNS")
    private String changeColumns;

    @Column(name = "SYS_CHANGE_CONTEXT")
    private String changeContext;

    @Column(name = "TransI_lgnNumber")
    @NaturalId
    private Long transNumber;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "vwPreparePickup_TransIlgnNumber", referencedColumnName = "TransI_lgnNumber", updatable = false, insertable = false, nullable = false)
    private List<PreparePickup> preparePickup;

    private static final long serialVersionUID = -8698628980236936422L;
}

