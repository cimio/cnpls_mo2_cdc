package com.cinepolis.mo.cdc.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
public class Version implements Serializable {

    @Id
    private Long version;

    private static final long serialVersionUID = 5954128611460390161L;
}
