package com.cinepolis.mo.cdc.model;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "vwPreparePickup")
public class PreparePickup implements Serializable {

    @Id
    @Column(name = "vwRowID")
    private Float id;

    @Column(name = "vwPreparePickup_lngNumber")
    private Long pickupOrder;

    @Column(name = "vwPreparePickup_intSequence")
    private Integer sequence;

    @Column(name = "vwPreparePickup_TransIlgnNumber")
    @NaturalId
    private Long transNumber;

    @Column(name = "vwPreparePickup_dtmDateTime")
    private Date preparePickup;

    @Column(name = "vwPreparePickup_strStatus")
    private Character status;

    @Column(name = "vwPreparePickup_intPickupOrder")
    private Long preparePickupOrder;

    @Column(name = "vwPreparePickup_strDescription")
    private String description;

    @Column(name = "vwPreparePickup_decQty")
    private Integer quantity;

    @Column(name = "vwPreparePickup_curValue")
    private Float curValue;

    @Column(name = "vwPrepatePickup_strWSSoldCode")
    private String dispatcher;

    @Column(name = "vwPreparePickup_strWSSold")
    private String channel;

    @Column(name = "vwPreparePickup_strWSPrepCode")
    private String code;

    @Column(name = "vwPreparePickup_strWSPrepared")
    private String prepared;

    @Column(name = "vwPreparePickup_strWSPickCode")
    private String pickupCode;

    @Column(name = "vwPreparePickup_strWSPickedUp")
    private String pickupUp;

    @Column(name = "vwPreparePickup_PrepareArea")
    private String kitchen;

    @Column(name = "vwPreparePickup_PickupArea")
    private String pickupArea;

    @Column(name = "vwPreparePickup_strFirstName")
    private String clientName;

    @Column(name = "vwPreparePickup_strLastName")
    private String clientLastName;

    @Column(name = "vwPreparePickup_intWaitTime")
    private Integer waitTime;

    @Column(name = "vwPreparePickup_strSplitActive")
    private Character splitActive;

    @Column(name = "vwPreparePickup_intManufTime")
    private Integer intManufTime;

    @Column(name = "vwPreparePickup_intDelvSeq")
    private Integer intDeliverySequence;

    @Column(name = "vwPreparePickup_strPrepPrt")
    private Character prepPrt;

    @Column(name = "vwPreparePickup_strPickPrt")
    private Character pickupPrt;

    @Column(name = "vwPreparePickup_strComment")
    private String comment;

    private static final long serialVersionUID = -5608289008038399765L;
}
