package com.cinepolis.mo.cdc.repository;

import com.cinepolis.mo.cdc.model.PreparePickup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PreparePickupRepository extends JpaRepository<PreparePickup, Long> {
    List<PreparePickup> findByTransNumber(Long order);
}
