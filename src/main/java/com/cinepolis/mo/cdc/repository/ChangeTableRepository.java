package com.cinepolis.mo.cdc.repository;

import com.cinepolis.mo.cdc.model.ChangeTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChangeTableRepository extends JpaRepository<ChangeTable, Long> {
    List<ChangeTable> findByTransNumber(Long order);
    List<ChangeTable> findByChangeOperationAndVersionGreaterThanEqual(Character changeOperation, Long version);
}
