package com.cinepolis.mo.cdc.repository;

import com.cinepolis.mo.cdc.model.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VersionRepository extends JpaRepository<Version, Long> {
    @Query(value = "select CHANGE_TRACKING_MIN_VALID_VERSION(OBJECT_ID('dbo.tblItemOrderStatus')) as version", nativeQuery = true)
    Version firstOne();

    @Query(value = "select CHANGE_TRACKING_CURRENT_VERSION()  as version", nativeQuery = true)
    Version lastOne();
}
