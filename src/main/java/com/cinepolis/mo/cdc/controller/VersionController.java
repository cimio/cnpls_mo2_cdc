package com.cinepolis.mo.cdc.controller;

import com.cinepolis.mo.cdc.model.Version;
import com.cinepolis.mo.cdc.service.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/versions")
public class VersionController {

    private VersionService versionService;

    @Autowired
    public VersionController(VersionService versionService) {
        this.versionService = versionService;
    }

    @GetMapping("/first")
    public Version getFirst() {
        return versionService.getFirstOne();
    }

    @GetMapping("/last")
    public Version getLast() {
        return versionService.getLastOne();
    }
}
