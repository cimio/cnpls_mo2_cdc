package com.cinepolis.mo.cdc.controller;

import com.cinepolis.mo.cdc.model.ChangeTable;
import com.cinepolis.mo.cdc.service.ChangeTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/changes")
public class ChangeTableController {

    private ChangeTableService changeTableService;

    @Autowired
    public ChangeTableController(ChangeTableService changeTableService) {
        this.changeTableService = changeTableService;
    }

    @GetMapping("/orders/{order}")
    public List<ChangeTable> getByOrder(@PathVariable Long order) {
        return changeTableService.getByOrder(order);
    }

    /**
     * Changes by version
     * @param version La última versión
     * @return List<ChangeTable>
     */
    @GetMapping("/{version}")
    public List<ChangeTable> getChanges(@PathVariable Long version) {
        return changeTableService.getChanges(version);
    }
}
