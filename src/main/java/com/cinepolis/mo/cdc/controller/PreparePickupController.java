package com.cinepolis.mo.cdc.controller;

import com.cinepolis.mo.cdc.model.PreparePickup;
import com.cinepolis.mo.cdc.service.PreparePickupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/prepare-pickups")
public class PreparePickupController {

    private PreparePickupService preparePickupService;

    @Autowired
    public PreparePickupController(PreparePickupService preparePickupService) {
        this.preparePickupService = preparePickupService;
    }
    
    @GetMapping("/orders/{order}")
    public List<PreparePickup> getOrder(@PathVariable Long order) {
        return preparePickupService.getOrder(order);
    }
    
    @PostMapping("/orders/{order}")
    public void sendOrderToKafka(@PathVariable Long order) {
        preparePickupService.publishOrderToKafka(order);
    }
}
