package com.cinepolis.mo.cdc.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {
	
	private static final Logger log = LoggerFactory.getLogger(DataSourceConfig.class);
	private DataSource ds = null;
	
	@Bean
	public DataSource getDataSource() {
		
		try {
			Properties properties = new Properties();
		    FileInputStream fileInputStream = new FileInputStream(new File("C:/CinemaNode/config/datasource/SQLServerDS.properties"));
		    
		    properties.load(fileInputStream);
		    fileInputStream.close();  
		    
		    try {
		    	ds = DataSourceBuilder
			    		  .create()
			    		  .username(properties.getProperty("cinepolis.DataSource.username", null))
			    		  .password(properties.getProperty("cinepolis.DataSource.password", null))
			    		  .url(properties.getProperty("cinepolis.DataSource.url", null))
			    		  .driverClassName(properties.getProperty("cinepolis.DataSource.driver", null))
			    		  .build();  
		    	
		    	ds.getConnection().isValid(500);
		      } catch(Exception ex) {
		    	  log.error("Error connecting to datasource: " + ex.getMessage());
		      }
		      
		} catch(IOException ex) {
			log.error("Error with file: " + ex.getMessage());
		}
		
		return ds;
 	}
}
