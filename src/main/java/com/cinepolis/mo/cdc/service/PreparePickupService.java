package com.cinepolis.mo.cdc.service;

import com.cinepolis.mo.cdc.configuration.AppUrlConstants;
import com.cinepolis.mo.cdc.model.PreparePickup;
import com.cinepolis.mo.cdc.repository.PreparePickupRepository;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PreparePickupService {

	private KafkaTemplate<String, String> orderProducer;
    private PreparePickupRepository preparePickupRepository;

    @Autowired
    public PreparePickupService(KafkaTemplate<String, String> orderProducer,
			PreparePickupRepository preparePickupRepository) {
		this.orderProducer = orderProducer;
		this.preparePickupRepository = preparePickupRepository;
	}

    public List<PreparePickup> getOrder(Long order) {
        return preparePickupRepository.findByTransNumber(order);
    }

    public void publishOrderToKafka(Long order) {
    	orderProducer.send(AppUrlConstants.ORDER_TOPIC, order.toString());
    	
    	log.info("Order sent: " + order);
    }
}
