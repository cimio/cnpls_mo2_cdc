package com.cinepolis.mo.cdc.service;

import com.cinepolis.mo.cdc.model.Version;
import com.cinepolis.mo.cdc.repository.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VersionService {

    private VersionRepository versionRepository;

    @Autowired
    public VersionService(VersionRepository versionRepository) {
        this.versionRepository = versionRepository;
    }

    public Version getFirstOne() {
        return versionRepository.firstOne();
    }

    public Version getLastOne() {
        return versionRepository.lastOne();
    }
}
