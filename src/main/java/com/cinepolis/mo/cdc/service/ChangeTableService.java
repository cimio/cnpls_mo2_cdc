package com.cinepolis.mo.cdc.service;

import com.cinepolis.mo.cdc.model.ChangeTable;
import com.cinepolis.mo.cdc.repository.ChangeTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChangeTableService {
    private static final Character CHANGE_OPERATION = 'I';
    private ChangeTableRepository changeTableRepository;

    @Autowired
    public ChangeTableService(ChangeTableRepository changeTableRepository) {
        this.changeTableRepository = changeTableRepository;
    }

    public List<ChangeTable> getByOrder(Long order) {
        return changeTableRepository.findByTransNumber(order);
    }

    public List<ChangeTable> getChanges(Long version) {
        return changeTableRepository.findByChangeOperationAndVersionGreaterThanEqual(CHANGE_OPERATION, version);
    }

}
